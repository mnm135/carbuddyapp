package com.example.emil.carbuddyapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Double mCarLatitude;
    Double mCarLongitude;
    Double mWalletLatitude;
    Double mWalletLongitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Switch serviceSwitch = (Switch) findViewById(R.id.main_activity_switch);
        serviceSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (tryToGetLocations()) {
                        if(isGPSturnedOn()){
                            Intent intent = new Intent(getApplicationContext(), LocationService.class);
                            Bundle bundle = new Bundle();
                            bundle.putDouble("carLat", mCarLatitude);
                            bundle.putDouble("carLon", mCarLongitude);
                            bundle.putDouble("walletLat", mWalletLatitude);
                            bundle.putDouble("walletLon", mWalletLongitude);
                            intent.putExtras(bundle);
                            startService(intent);
                        } else {
                            showToast(getResources().getString(R.string.toast_no_gps));
                            openGPSsettings();
                        }
                    } else {
                        showToast(getResources().getString(R.string.toast_incorrect_data));
                        openLocationSettings();
                    }
                } else {
                    stopService();
                }
            }
        });
    }

    protected void showToast(String text) {
        Toast.makeText(getApplicationContext(),
                text,
                Toast.LENGTH_LONG)
                .show();
    }

    protected boolean isGPSturnedOn() {
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    protected void openGPSsettings() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }

    protected void openLocationSettings() {
        Intent i = new Intent(this, SettingsActivity.class);
        startActivity(i);
    }


    protected boolean tryToGetLocations() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String carLatitude = sharedPreferences.getString(getResources().
                getString(R.string.pref_car_latitude_key), null);
        String carLongitude = sharedPreferences.getString(getResources().getString(R.string.pref_car_longitude_key), null);
        String walletLatitude = sharedPreferences.getString(getResources().
                getString(R.string.pref_wallet_latitude_key), null);
        String walletLongitude = sharedPreferences.getString(getResources().
                getString(R.string.pref_wallet_longitude_key), null);

        try {
            mWalletLatitude = Double.parseDouble(walletLatitude);
            mWalletLongitude = Double.parseDouble(walletLongitude);
            mCarLatitude = Double.parseDouble(carLatitude);
            mCarLongitude = Double.parseDouble(carLongitude);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void stopService() {
        stopService(new Intent(this, LocationService.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_location_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
