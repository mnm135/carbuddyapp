package com.example.emil.carbuddyapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import static com.google.android.gms.location.places.ui.PlacePicker.getPlace;

public class SettingsActivity extends PreferenceActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new LocationPreferenceFragment()).commit();
    }

    public static class LocationPreferenceFragment extends PreferenceFragment
     implements SharedPreferences.OnSharedPreferenceChangeListener {

        public static final String TAG = "SettingsFragment";
        public static final int PLACE_PICKER_CAR_REQUEST_CODE = 1;
        public static final int PLACE_PICKER_WALLET_REQUEST_CODE =  2;

        Preference.OnPreferenceClickListener preferenceClickListener = new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                String key = preference.getKey();
                if (key.equals(getResources().getString(R.string.pref_wallet_button_key))) {
                    startLocationFragment(PLACE_PICKER_WALLET_REQUEST_CODE);
                }
                else if (key.equals(getResources().getString(R.string.pref_car_button_key))) {
                    startLocationFragment(PLACE_PICKER_CAR_REQUEST_CODE);
                }
                return true;
            }
        };

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.pref_general);

            Preference carLocationButton = findPreference(getString(R.string.pref_car_button_key));
            Preference walletLocationButton = findPreference(getString(R.string.pref_wallet_button_key));
            carLocationButton.setOnPreferenceClickListener(preferenceClickListener);
            walletLocationButton.setOnPreferenceClickListener(preferenceClickListener);

            displaySummaryOnStart();
        }

        public void startLocationFragment(int code) {
            try {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                startActivityForResult(builder.build(getActivity()), code);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            Place place = getPlace(getActivity(), data);
            LatLng latLng = place.getLatLng();
            double latitude = latLng.latitude;
            double longitude = latLng.longitude;

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            sharedPreferences.registerOnSharedPreferenceChangeListener(this);

            if (requestCode == PLACE_PICKER_CAR_REQUEST_CODE) {
                if (resultCode == Activity.RESULT_OK) {
                    sharedPreferences.edit().putString(getResources()
                            .getString(R.string.pref_car_longitude_key), String.valueOf(longitude)).apply();
                    sharedPreferences.edit().putString(getResources()
                            .getString(R.string.pref_car_latitude_key), String.valueOf(latitude)).apply();
                }
            }
            else if (requestCode == PLACE_PICKER_WALLET_REQUEST_CODE) {
                if (resultCode == Activity.RESULT_OK) {
                    sharedPreferences.edit().putString(getResources()
                            .getString(R.string.pref_wallet_longitude_key), String.valueOf(longitude)).apply();
                    sharedPreferences.edit().putString(getResources()
                            .getString(R.string.pref_wallet_latitude_key), String.valueOf(latitude)).apply();
                }
            }
        }

        @Override
        public void onResume(){
            super.onResume();
            getPreferenceManager().getSharedPreferences()
                    .registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            getPreferenceManager().getSharedPreferences()
                    .unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences preference, String key) {
            Preference pref = findPreference(key);
            pref.setSummary(preference.getString(key, ""));

            Log.i(TAG, "preference key " + pref.getKey());
            Log.i(TAG, "preference summary " + pref.getSummary());
        }

        public void displaySummaryOnStart() {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            String carLatitudePreference = sharedPreferences.getString(getResources().getString(R.string.pref_car_latitude_key), "NULL");
            String carLongitudePreference = sharedPreferences.getString(getResources().getString(R.string.pref_car_longitude_key), "NULL");
            String walletLatitudePreference = sharedPreferences.getString(getResources().getString(R.string.pref_wallet_latitude_key), "NULL");
            String walletLongitudePreference = sharedPreferences.getString(getResources().getString(R.string.pref_wallet_longitude_key), "NULL");

            EditTextPreference carLatitudeEditTextPreference = (EditTextPreference) findPreference(getResources().getString(R.string.pref_car_latitude_key));
            EditTextPreference carLongitudeEditTextPreference = (EditTextPreference) findPreference(getResources().getString(R.string.pref_car_longitude_key));
            EditTextPreference walletLatitudeEditTextPreference = (EditTextPreference) findPreference(getResources().getString(R.string.pref_wallet_latitude_key));
            EditTextPreference walletLongitudeEditTextPreference = (EditTextPreference) findPreference(getResources().getString(R.string.pref_wallet_longitude_key));

            carLatitudeEditTextPreference.setSummary(carLatitudePreference);
            carLongitudeEditTextPreference.setSummary(carLongitudePreference);
            walletLatitudeEditTextPreference.setSummary(walletLatitudePreference);
            walletLongitudeEditTextPreference.setSummary(walletLongitudePreference);
        }
    }
}
